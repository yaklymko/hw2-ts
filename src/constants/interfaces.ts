export interface IFighter extends Object {
    readonly _id: string;
    readonly name: string;
             health: number;
    readonly attack: number;
    readonly defense: number;
    readonly source: string;
}

export interface IFighterBattle extends IFighter {
    isBlocking: boolean,
    superAttackCodes : KeyCode[],
    hitBarPart : number,
    hasSuperAttack : boolean,
    resolve : Function,
}

export interface IBattleInfo {
    isBlocking?: boolean,
    superAttackCodes?: string[],
}

export type KeyCode = "KeyA" | 'KeyD' | 'KeyJ' | 'KeyL' | 'KeyQ' | 'KeyW' | 'KeyE' | 'KeyU' | 'KeyI' | 'KeyO';

export interface IControls {
    PlayerOneAttack: KeyCode,
    PlayerOneBlock: KeyCode,
    PlayerTwoAttack: KeyCode,
    PlayerTwoBlock: KeyCode,
    PlayerOneCriticalHitCombination: KeyCode[],
    PlayerTwoCriticalHitCombination: KeyCode[]
}