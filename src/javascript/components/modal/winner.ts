import {showModal} from './modal';
import {IFighter} from "../../../constants/interfaces";

export function showWinnerModal(fighter : IFighter) {
  showModal({
    title: "We have a winner!",
    bodyElement: `Fighter ${fighter.name} won!`
  });
}
