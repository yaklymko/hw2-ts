import {controls} from '../../constants/controls';
import {IFighter, IFighterBattle} from "../../constants/interfaces";

export async function fight(firstFighterArg: IFighter, secondFighterArg: IFighter): Promise<IFighter> {

    return new Promise((resolve) => {
        document.addEventListener("keydown", keyDownEvent);
        document.addEventListener("keyup", keyUpEvent);

        const firstFighter: IFighter & IFighterBattle = {
            ...firstFighterArg,
            isBlocking: false,
            superAttackCodes: controls.PlayerOneCriticalHitCombination,
            hitBarPart: 100 / firstFighterArg.health,
            hasSuperAttack: true,
            resolve: resolve
        };

        const secondFighter: IFighter & IFighterBattle = {
            ...secondFighterArg,
            isBlocking: false,
            superAttackCodes: controls.PlayerTwoCriticalHitCombination,
            hitBarPart: 100 / secondFighterArg.health,
            hasSuperAttack: true,
            resolve: resolve
        };
        const pressed = new Set();
        // I'd use copy but it can't pass tests

        function keyDownEvent(event: KeyboardEvent) {
            if (event.repeat) {
                // checks if a key is being held down
                return;
            }

            pressed.add(event.code);

            if (checkSuperAttacks(firstFighter, secondFighter)) {
                renderHitbars(firstFighter, secondFighter);
                return;
            }

            switch (event.code) {

                case controls.PlayerOneAttack: {
                    getDamage(firstFighter, secondFighter);
                }
                    break;

                case controls.PlayerTwoAttack: {
                    getDamage(secondFighter, firstFighter);
                }
                    break;

                case controls.PlayerOneBlock: {
                    firstFighter.isBlocking = true;
                }
                    break;

                case controls.PlayerTwoBlock: {
                    secondFighter.isBlocking = true;
                }
                    break;

            }
            renderHitbars(firstFighter, secondFighter);
        }

        function keyUpEvent(event: KeyboardEvent) {
            pressed.delete(event.code);
            switch (event.code) {
                case controls.PlayerOneBlock: {
                    firstFighter.isBlocking = false;
                }
                    break;

                case controls.PlayerTwoBlock: {
                    secondFighter.isBlocking = false;
                }
                    break;
            }
        }

        function checkSuperAttacks(firstFighter: IFighterBattle, secondFighter: IFighterBattle) {
            if (isSuperAttack(firstFighter) && firstFighter.hasSuperAttack) {
                firstFighter.hasSuperAttack = false;
                setTimeout(() => {
                    firstFighter.hasSuperAttack = true;
                }, 10000);
                getCritDamage(firstFighter, secondFighter);
            }

            if (isSuperAttack(secondFighter) && secondFighter.hasSuperAttack) {
                secondFighter.hasSuperAttack = false;
                setTimeout(() => {
                    secondFighter.hasSuperAttack = true;
                }, 10000);
                getCritDamage(secondFighter, firstFighter);
            }
            return false
        }

        function isSuperAttack(fighter: IFighterBattle) {
            for (let code of fighter.superAttackCodes) {
                if (!pressed.has(code)) {
                    return;
                }
            }
            return true;
        }
    })

}

export function getCritDamage(attacker: IFighter & IFighterBattle, defender: IFighter & IFighterBattle) {
    // Render hitbar
    let dmg = getCritHitPower(attacker);
    defender.health -= dmg;

    if (defender.health <= 0) {
        defender.health = 0;
        attacker.resolve(attacker);
    }
    return dmg;
}

export function renderHitbars(firstFighter: IFighter & IFighterBattle, secondFighter: IFighter & IFighterBattle) {
    (<HTMLElement>document.getElementById("left-fighter-indicator")).style.width = firstFighter.health * firstFighter.hitBarPart + "%";
    (<HTMLElement>document.getElementById("right-fighter-indicator")).style.width = secondFighter.health * secondFighter.hitBarPart + "%";
}

export function getCritHitPower(fighter: IFighter) {
    return fighter.attack * 2;
}

export function getDamage(attacker: IFighter & IFighterBattle, defender: IFighter & IFighterBattle) {
    if (defender.isBlocking || attacker.isBlocking) {
        return;
    }
    let dmg = getHitPower(attacker) - getBlockPower(defender);
    if (dmg < 0) {
        dmg = 0;
    }
    defender.health -= dmg;

    if (defender.health <= 0) {
        defender.health = 0;
        attacker.resolve(attacker);
    }

    return dmg;
}

export function getHitPower(fighter: IFighter) {
    return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter: IFighter) {
    return fighter.defense * (Math.random() + 1);
}