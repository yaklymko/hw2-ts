import {createElement} from '../helpers/domHelper';
import {IFighter} from '../../constants/interfaces';

export function createFighterPreview(fighter: IFighter, position: string): HTMLElement | string {
    if (!fighter) {
        return "";
    }
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });


    const imageElement = createFighterImage(fighter);
    const fighterDescription = createFighterDescription(fighter);

    fighterElement.append(fighterDescription);
    fighterElement.append(imageElement);

    return fighterElement;
}

function createFighterDescription(fighter: IFighter) {

    const fighterStats: Record<string, number> = {
        "health": fighter.health || 0,
        "attack": fighter.attack || 0,
        "defense": fighter.attack || 0,
    }

    const statsList = createElement({
        tagName: "span",
        className: 'fighter-preview___stats',
    });

    for (let statName of Object.keys(fighterStats)) {

        const statValue: string = fighterStats[statName].toString();
        const statsListItem = createElement({
            tagName: "p",
            className: ""
        });

        statsListItem.textContent = `${statName.charAt(0).toUpperCase() + statName.slice(1)} : [${statValue}]`;
        statsList.appendChild(statsListItem);
    }
    return statsList;

}

export function createFighterImage(fighter : IFighter) :HTMLElement {
    const {
        source,
        name
    } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };

    return createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

}