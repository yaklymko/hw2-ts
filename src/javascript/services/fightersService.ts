import {callApi} from '../helpers/apiHelper';
import {IFighter} from "../../constants/interfaces";

class FighterService {
    async getFighters() {
        try {
            const endpoint = 'fighters.json';

            return <IFighter[]>(await callApi(endpoint, 'GET'));

        } catch (error) {
            throw error;
        }
    }

    async getFighterDetails(id: number) {
        try {
            const endpoint = `details/fighter/${id}.json`;
            return await callApi(endpoint, 'GET');

        } catch (error) {
            throw error;
        }

    }
}

export const fighterService = new FighterService();
